#!/bin/bash
set -e

PROJECT_ID="50459724"
ENTRY_POINT="examples/run-task.sh"

# read token from the working directory
REPO_TOKEN=""
if [[ -f repo_token ]]; then
  REPO_TOKEN=$(cat repo_token)
fi

# version is passed as first argument
COMMIT=$(echo $1 | tr -cd '[:alnum:]_-')
if [[ "$COMMIT" == "" ]]; then
    COMMIT=master
fi

# job dir should be empty, but for debugging purposes we could rerun the script, so clean it up
if [[ -f archive.tar.gz ]]; then
  rm archive.tar.gz
fi

echo "Downloading artifact for version" $COMMIT

set +e
curl -f -s -S --header "PRIVATE-TOKEN: ${REPO_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/jobs/artifacts/${COMMIT}/raw/artifacts/source.tar.gz?job=test" > archive.tar.gz
RET=$?
set -e
if [[ "$RET" != "0" ]]; then
  echo "Failed to download artifact. Downloading repository archive instead." >&2
  curl -f -s -S --header "PRIVATE-TOKEN: ${REPO_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/archive.tar.gz?sha=${COMMIT}" > archive.tar.gz
  curl -f -s -S --header "PRIVATE-TOKEN: ${REPO_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/commits/${COMMIT}" > version.json
fi
tar -xvf archive.tar.gz --strip 1
#chown -R ubuntu:ubuntu .

# Load environment variables if the image was customized
. /etc/environment

# Execute the entry point
bash "$ENTRY_POINT" "$@"
