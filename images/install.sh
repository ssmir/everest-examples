#!/bin/bash -e
sudo apt-get -y update --fix-missing
sudo apt-get -y install python3-pip
sudo apt-get -y install policykit-1
sudo pip3 install pyomo tornado==4.5.3 psutil
