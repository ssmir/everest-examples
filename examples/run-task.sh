#!/bin/bash
set -e

DIR=$(dirname $0)
COMMIT="$1"

# ... prepare input files ...

python3 $DIR/parse_version.py "$DIR/vesion.json" "$COMMIT" > version.txt

LANG=en_US.UTF-8 python3 -u $DIR/main.py
RET=$?

# ... prepare output files ...

exit $RET
