import datetime
import json
import sys
with open(sys.argv[1], 'r') as f:
    j=json.load(f)
if 'commit' in j:
    j = j['commit']
d=datetime.datetime.fromisoformat(j['created_at'].replace('Z', '+00:00'))
s=(datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)-d).total_seconds()
seconds=int(s)
minutes=int(s/60)
hours=int(s/3600)
days=int(s/3600/60)
if days:
    elapsed = "%d days" % days
elif hours:
    elapsed = "%d hours" % hours
elif minutes:
    elapsed = "%d minutes" % minutes
else:
    elapsed = "%d seconds" % seconds
ver = j['short_id']
if len(sys.argv) >= 3:
    branch = sys.argv[2]
    if not j['short_id'] in branch:
        ver = '%s, %s' % (branch, j['short_id'])
print('%s ago: %s (%s) by %s at %s' % (
    elapsed, j['title'], ver,
    j['author_name'], j['created_at']))
