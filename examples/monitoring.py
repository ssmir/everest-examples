import os
import time
import socket
import struct

CONN = None

class MonitoringConnection(object):

    def __init__(self, address, port, task_id):
        self.address = address
        self.port = port
        self.task_id = task_id
        self.sock = None
        self.connected = False
        self.prefix = ''
        self.stage = '0'

    def connect(self):
        self.connected = False
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((self.address, self.port))
            self.sock.sendall(struct.pack('>Ib', len(self.task_id) + 1, 0))
            self.sock.sendall(self.task_id.encode('utf-8'))
            self.connected = True
        except socket.error as err:
            print("Error connecting to Everest agent:", err)
            self.close()

    def send(self, msg):
        with open('current-info.txt', 'w', encoding='utf-8') as f:
            f.write(msg)
        if not self.connected:
            return
        sent = False
        while not sent:
            try:
                self.sock.sendall(struct.pack('>I', len(msg)))
                self.sock.sendall(msg.encode('utf-8'))
                sent = True
            except (RuntimeError, socket.error) as err:
                print("Error while sending status to Everest agent:", err)
                time.sleep(5)
                self.close()
                self.connect()

    def set_prefix(self, prefix):
        self.prefix = prefix
    
    def set_stage(self, stage):
        self.stage = stage

    def report_state(self, msg):
        self.send('INFO [%s] %s%s' % (self.stage, self.prefix, msg))

    def close(self):
        if self.sock is not None:
            self.sock.close()

class MockConnection(object):
    def __init__(self):
        self.prefix = ''
        self.stage = '0'

    def connect(self):
        pass

    def set_prefix(self, prefix):
        self.prefix = prefix

    def set_stage(self, stage):
        self.stage = stage

    def report_state(self, msg):
        print("MON [%s] %s%s" % (self.stage, self.prefix, msg))

    def close(self):
        pass

def get_connection():
    global CONN
    if CONN is None:
        port = int(os.environ.get('EVEREST_AGENT_PORT', 0))
        if port != 0:
            print("Agent port: %d" % port)
            address = os.environ.get('EVEREST_AGENT_ADDRESS', 'localhost')
            print("Agent address: %s" % address)
            taskId = os.environ.get('EVEREST_AGENT_TASK_ID', 'xxx')
            print("TASK_ID: %s" % taskId)
            CONN = MonitoringConnection(address, port, taskId)
            CONN.connect()
        else:
            CONN = MockConnection()
    return CONN
