import time

import monitoring

start = time.time()

print('Hello world!')

monitoring.get_connection().report_state('[%f] Starting up...' % (time.time() - start))

for i in range(5):
    time.sleep(5)
    print('After 5s')
    monitoring.get_connection().report_state('[%f] Waiting 5s...' % (time.time() - start))
